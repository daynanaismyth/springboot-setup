package tutorial.springboot.service;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tutorial.springboot.entity.Player;
import tutorial.springboot.repository.PlayerRepository;

@Service
@Transactional
public class PlayerService {
	
	@Autowired
	private PlayerRepository playerRepo;
	
	/**
	 * Find a player by it's id
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Player findPlayerById(Long id) throws Exception{
		if(id == null){
			throw new Exception("Id cannot be null.");
		}
		
		return playerRepo.findOne(id);
	}
	
	/**
	 * Create a new player
	 * @param player
	 * @return
	 * @throws Exception 
	 */
	public Player createNewPlayer(Player player) throws Exception{
		if(player == null){
			throw new Exception("Player cannot be null.");
		}
		
		return playerRepo.save(player);
	}
}
