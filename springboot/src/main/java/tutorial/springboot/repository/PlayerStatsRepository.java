package tutorial.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tutorial.springboot.entity.PlayerStats;


public interface PlayerStatsRepository extends JpaRepository<PlayerStats, Long> {

}
