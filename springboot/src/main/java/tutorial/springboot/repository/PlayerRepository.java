package tutorial.springboot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import tutorial.springboot.entity.Player;
import tutorial.springboot.entity.PlayerStats;
import tutorial.springboot.entity.Team;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {

}
