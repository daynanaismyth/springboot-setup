package tutorial.springboot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

// Table to store overall player stats
@Table(name="player_stats")
@Entity
public class PlayerStats {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	// Store their overall goal count
	@Column(name="goal_count")
	private Long goalCount;

	// Store their overall assist count
	@Column(name="assist_count")
	private Long assistCount;
	
	// Store their overall point count
	@Column(name="point_count")
	private Long pointCount;
	
	public PlayerStats(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGoalCount() {
		return goalCount;
	}

	public void setGoalCount(Long goalCount) {
		this.goalCount = goalCount;
	}

	public Long getAssistCount() {
		return assistCount;
	}

	public void setAssistCount(Long assistCount) {
		this.assistCount = assistCount;
	}

	public Long getPointCount() {
		return pointCount;
	}

	public void setPointCount(Long pointCount) {
		this.pointCount = pointCount;
	}
	
}
