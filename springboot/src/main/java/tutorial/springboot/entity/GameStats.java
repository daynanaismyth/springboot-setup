package tutorial.springboot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import tutorial.springboot.dto.StatsType;

// Store game stats separately from player stats
@Table(name="game_stats")
@Entity
public class GameStats {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne	// One player may have many stats for one game
	private Player player;
	
	@ManyToOne	// Many GameStats to One Game
	private Game game;
	
	@Enumerated(EnumType.STRING)
	@Column(name="type")
	private StatsType type;
	
	public GameStats(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public StatsType getType() {
		return type;
	}

	public void setType(StatsType type) {
		this.type = type;
	}
}
