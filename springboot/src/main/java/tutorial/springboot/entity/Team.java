package tutorial.springboot.entity;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.JoinColumn;


@Entity
@Table(name="team")
public class Team {
	
	@Id	// the Id annotation automatically creates the primary key.
    @GeneratedValue(strategy = GenerationType.AUTO)	// rather then manually setting an id each time we create a user, allow for it to be auto generated
	private Long id;

	@Column(name="name")
	private String name;
	
	@Column(name="city")
	private String city;
	
	@Column(name="division")
	private String division;
	
	@Column(name="coach")
	private String coach;
	
	public Team(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getCoach() {
		return coach;
	}

	public void setCoach(String coach) {
		this.coach = coach;
	}
}
