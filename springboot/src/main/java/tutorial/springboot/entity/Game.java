package tutorial.springboot.entity;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="game")
public class Game {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@OneToOne
	private Team homeTeam;
	
	@OneToOne
	private Team awayTeam;
	
	@Column(name="date")	// store the date & time of the game
	private ZonedDateTime date;
	
	// One game will have one-to-many stats
	@OneToMany
	private List<GameStats> gameStats = new ArrayList<GameStats>();
	
	public Game(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Team getHomeTeam() {
		return homeTeam;
	}

	public void setHomeTeam(Team homeTeam) {
		this.homeTeam = homeTeam;
	}

	public Team getAwayTeam() {
		return awayTeam;
	}

	public void setAwayTeam(Team awayTeam) {
		this.awayTeam = awayTeam;
	}

	public ZonedDateTime getDate() {
		return date;
	}

	public void setDate(ZonedDateTime date) {
		this.date = date;
	}

	public List<GameStats> getGameStats() {
		return gameStats;
	}

	public void setGameStats(List<GameStats> gameStats) {
		this.gameStats = gameStats;
	}	
	
	
}

