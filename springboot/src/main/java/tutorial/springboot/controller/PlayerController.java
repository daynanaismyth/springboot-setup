package tutorial.springboot.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tutorial.springboot.entity.Player;
import tutorial.springboot.service.PlayerService;

@RestController
@RequestMapping("/api")
public class PlayerController {
	
	@Autowired
	private PlayerService playerService;
	
	/**
	 * Find overall player stats by the player id
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/players/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Player findPlayerById(@PathVariable("id") Long id) throws Exception{
		return playerService.findPlayerById(id);
	}
	
	/**
	 * Create a new player
	 * @param player
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/players", method = RequestMethod.POST)
	@ResponseBody
	public Player createNewPlayer(@RequestBody final Player player) throws Exception{
		return playerService.createNewPlayer(player);
	}
}
